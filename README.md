**Maven(Java required)**

* Install [Maven](https://maven.apache.org/download.cgi)
* Configure path variables


**Project**
* Clone the project
* Install the dependecies of pom.xml file `mvn install` 

**Postgres Required**

* Download it in your computer or yu can run it through a* [ ]  docker container(Docker Required) 
* [Postgres](https://www.postgresql.org/download/)
* [Docker](https://www.docker.com/products/docker-desktop)


**PgAdmin**
* For manage postgres database with ui
* [PgAdmin](https://www.pgadmin.org/download/)


**Configure postgres db connection**

* Configure application.properties file in project. 

**Run springboot project**

* `mvn spring-boot:run` 