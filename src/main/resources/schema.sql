DROP TABLE IF EXISTS Animal;

CREATE TABLE Animal (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name varchar(255),
    description varchar(255)
);