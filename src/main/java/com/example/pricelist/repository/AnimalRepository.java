package com.example.pricelist.repository;

import java.util.List;

import com.example.pricelist.entity.Animal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AnimalRepository extends JpaRepository<Animal, Long> {


    
}