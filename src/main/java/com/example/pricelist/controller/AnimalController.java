package com.example.pricelist.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.pricelist.TestResponse;
import com.example.pricelist.entity.Animal;
import com.example.pricelist.exception.ResourceNotFoundException;
import com.example.pricelist.repository.AnimalRepository;
import com.example.pricelist.service.AnimalService;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/")
public class AnimalController{

//    private final AnimalService animalService;
    private static final String template = "Hello, %s!";
    @Autowired
    private AnimalRepository animalRepository;

    @GetMapping("/greeting")
    public TestResponse greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new TestResponse(1, String.format(template, name));
    }

    @RequestMapping("animals")
    public List<Animal> getAll() {
        return animalRepository.findAll();
    }

    @RequestMapping("/animals/{id}")
    public ResponseEntity<Animal> getAnimalById(@PathVariable(value = "id") Long id)
            throws ResourceNotFoundException {
        Animal animal = animalRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Animal not found for id :: "+ id));
        return ResponseEntity.ok().body(animal);
    }

    @PostMapping("animals")
    public Animal createAnimal(@RequestBody Animal animal){
        return this.animalRepository.save(animal);
    }

    @PutMapping("animals/{id}")
    public ResponseEntity<Animal> updateAnimal(@PathVariable(value = "id") Long id, @Valid @RequestBody Animal updatedAnimal)
            throws ResourceNotFoundException {
        Animal animal = this.animalRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Animal not found for id :: "+ id));

        animal.setName(updatedAnimal.getName());
        animal.setDescription(updatedAnimal.getDescription());

        return ResponseEntity.ok(this.animalRepository.save(animal));
    }

    @DeleteMapping("animals/{id}")
    public Map<String, Boolean> deleteAnimal(@PathVariable(value = "id") Long id)
            throws ResourceNotFoundException {
        Animal animal = animalRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Animal not found for id :: "+ id));

        this.animalRepository.delete(animal);

        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);

        return response;
    }


}